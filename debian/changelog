omvviewer (1.22.11-1) unstable; urgency=low

  * New Upstream Version

 -- Robin Cornelius <robin.cornelius@gmail.com>  Fri, 13 Mar 2009 09:56:30 +0000

omvviewer (1.22.10-1) experimental; urgency=low

  * New Upstream Version

 -- Robin Cornelius <robin.cornelius@gmail.com>  Thu, 26 Feb 2009 21:10:43 +0000

omvviewer (1.22.9-2) experimental; urgency=low

  * Fix a crash issue with a patch

 -- Robin Cornelius <robin.cornelius@gmail.com>  Sat, 14 Feb 2009 12:57:28 +0000

omvviewer (1.22.9-1) experimental; urgency=low

  * New Upstream Version

 -- Robin Cornelius <robin.cornelius@gmail.com>  Fri, 13 Feb 2009 18:11:56 +0000

omvviewer (1.22.8-1) experimental; urgency=low

  * New Upstream Version
    + Drop OpenAL patch, included upstream

 -- Robin Cornelius <robin.cornelius@gmail.com>  Thu, 05 Feb 2009 22:27:02 +0000

omvviewer (1.22.7-1) experimental; urgency=low

  * New Upstream Version
    + New patches
      - AvatarLoginList
      - VWR-11663_Save_Load_scripts
      - Dale Glass' avatar scanner
      - VWR 11103 Fix local ruler on linked objects
      - VWR 11674 don't download muted sounds
      - VWR 11128 Correctly Find pythong during build
      - VWR 11138 Fix build system

 -- Robin Cornelius <robin.cornelius@gmail.com>  Thu, 29 Jan 2009 10:51:56 +0000

omvviewer (1.22.5-1) experimental; urgency=low

  * New Upstream Version

 -- Robin Cornelius <robin.cornelius@gmail.com>  Thu, 08 Jan 2009 12:38:41 +0000

omvviewer (1.22.4-1) experimental; urgency=low

  * New Upstream Version
    + Add missing SDL cursors

 -- Robin Cornelius <robin.cornelius@gmail.com>  Sat, 20 Dec 2008 18:03:56 +0000

omvviewer (1.22.2-1) experimental; urgency=low

  * New Upstream Version

 -- Robin Cornelius <robin.cornelius@gmail.com>  Sat, 06 Dec 2008 17:46:10 +0000

omvviewer (1.22.1-1) experimental; urgency=low

  * New Upstream RC Version
    + Change depends to require a cares enabled libcurl
    + Rework debian/rules to allow debug flags to be passed to build
  * New Patches
    + Avatar scanner floater
    + VWR-9557-EnableBuildWithNvidiaOrMesaHeaders
    + correctly_find_dbus_headers_on_standalone
  * Drop patches applied upstream
    + VWR4070-openjpeg_check_number_of_components
    + rename_binary_cmake
    + no_old_school_typedefs

 -- Robin Cornelius <robin.cornelius@gmail.com>  Tue, 25 Nov 2008 21:16:52 +0000

omvviewer (1.22.0-1) unstable; urgency=low

  * New Upstream Version

 -- Robin Cornelius <robin.cornelius@gmail.com>  Sun, 23 Nov 2008 16:43:59 +0000

omvviewer (1.21.6-1) unstable; urgency=low

  * New Upstream Version
    + DEV-21775 is now upstream, remove patch
  * Import OMVV-1 linux joystick support patch
  * Update standards version
  * Fix missing icon  

 -- Robin Cornelius <robin.cornelius@gmail.com>  Sat, 18 Oct 2008 10:32:21 +0100

omvviewer (1.21.4-2) unstable; urgency=high

  * Security release, actualy version 1.21.5 but upgraded by patch from upstream
    + Fixes DEV-21775 stop abartary file upload from users PC

 -- Robin Cornelius <robin.cornelius@gmail.com>  Tue, 07 Oct 2008 16:32:57 +0100

omvviewer (1.21.4-1) unstable; urgency=low

  * New Upstream Version

 -- Robin Cornelius <robin.cornelius@gmail.com>  Fri, 03 Oct 2008 19:13:35 +0100

omvviewer (1.21.3-1) unstable; urgency=high

  * New Upstream Version
    + Security alert fix

 -- Robin Cornelius <robin.cornelius@gmail.com>  Fri, 26 Sep 2008 21:12:36 +0100

omvviewer (1.21.2-1) unstable; urgency=low

  * New Upstream Version
    + Import glh_linear.h as a patch
    + Add replacment viewer pixmap as a patch
    + rebase 24_always_test_vectorize.diff 
    + remove --as-needed on gcc line, works around broken cmake 2.6.X < 2.6.2
    + drop add_find_curl_cmake.patch, not needed

 -- Robin Cornelius <robin.cornelius@gmail.com>  Sun, 14 Sep 2008 17:00:28 +0100

omvviewer (1.21.1-1) unstable; urgency=low

  * New Upstream Version
    + Drop all existing patches
    + Convert any patches that still apply to quilt
    + Fix cmake build for standalone systems
    + Rebase openal for cmake
    + update depends and control for a debian lenny target system

 -- Robin Cornelius <robin.cornelius@gmail.com>  Mon, 08 Sep 2008 22:11:47 +0100

omvviewer (1.21.0-1) unstable; urgency=low

  * New Upstream Version

 -- Robin Cornelius <robin.cornelius@gmail.com>  Tue, 07 Sep 2008 10:20:11 +0100

omvviewer (1.20.15-1) unstable; urgency=low

  * New Upstream Version, 
    + Add 0000_no_idle_memcheck.dpatch

 -- Robin Cornelius <robin.cornelius@gmail.com>  Fri, 25 Jul 2008 13:35:23 +0200

omvviewer (1.20.14-1) experimental; urgency=low

  * New Upstream Version
    + New patch VWR-5917_multiple_user_day_cycles_in_rw_area.dpatch 
    + Modify VWR-4981_save_windlight_settings_in_user_rw_area.dpatch 
    + Bump artwork dependencies
    + New patch VWR-8194_clamp_outline_for_broken_nvidia.dpatch

 -- Robin Cornelius <robin.cornelius@gmail.com>  Wed, 16 Jul 2008 23:11:35 +0200

omvviewer (1.20.13-1) experimental; urgency=low

  * New Upstream Version
    + Fix some patches for file path changes    
      - Modify VWR-5082_set_bulk_inv_permissions.dpatch
      - Modify trademark_compliance.dpatch
    + Add VWR-4981_save_windlight_settings_in_user_rw_area.dpatch 

 -- Robin Cornelius <robin.cornelius@gmail.com>  Sat, 12 Jul 2008 22:15:56 +0200

omvviewer (1.20.12-2) experimental; urgency=low

  * Fix packaging mistake that resulted in clouds2.tga appearing in wrong package

 -- Robin Cornelius <robin.cornelius@gmail.com>  Wed, 09 Jul 2008 12:24:28 +0100

omvviewer (1.20.12-1) experimental; urgency=low

  * New Upstream Version
    + Add VWR-5082_set_bulk_inv_permissions.dpatch

 -- Robin Cornelius <robin.cornelius@gmail.com>  Tue, 08 Jul 2008 12:20:50 +0100

omvviewer (1.20.11-1) experimental; urgency=low

  * New Upstream Version
    + Add VWR-7831_smart_pointer_explicit_casting.dpatch

 -- Robin Cornelius <robin.cornelius@gmail.com>  Thu, 26 Jun 2008 15:45:27 +0100

omvviewer (1.20.10-1) experimental; urgency=low

  * New Upstream Version
    + Drop patches/oldpatch/VWR1352_fix_strange_llstyle_copy_operation.dpatch
  * Fix watch file

 -- Robin Cornelius <robin.cornelius@gmail.com>  Tue, 24 Jun 2008 14:01:30 +0100

omvviewer (1.20.9-1) experimental; urgency=low

  * New Upstream Version

 -- Robin Cornelius <robin.cornelius@gmail.com>  Mon, 09 Jun 2008 19:40:54 +0100

omvviewer (1.20.8-1) experimental; urgency=low

  * New Upstream Version
    Rename package omvviewer for trademark compliance

 -- Robin Cornelius <robin.cornelius@gmail.com>  Thu, 29 May 2008 12:36:33 +0100

slviewer (1.20.7-1) experimental; urgency=low

  * New upstream release
  * Patchs applied upstream so we drop them here
    + Drop VWR3605_Object_group_display_not_properly_set_for_no-group_objects.dpatch
    + Drop VWR2142_parcel_voice_icon_doesnt_reflect_status_if_voice_is_not_used.dpatch
    + Drop VWR3857_show_non_latin1_characters_in_lsl_editor.dpatch
  * New patches
    + Add VWR-5697_fix_startup_paths.dpatch
  * rebase patches
    + 21_VWR_2488_Standalone_build_fixes.dpatch
    + 75_use_debian-included_fonts.dpatch
    + 55_fix_the_locales.dpatch

 -- Robin Cornelius <robin.cornelius@gmail.com>  Thu, 22 May 2008 08:56:04 +0100

slviewer (1.20.6-1) experimental; urgency=low

  * New upstream release
  * Patch changes
    + Fix texture corruption issue for lossess and map textures etc.
      - add VWR-1815_top_corner_fix.dpatch
    + Drop dont_default_voice_on.dpatch due to changes in settings storage
    + Possible crash in llviewerpartsim
      - add 0001_possible_crash_in_llviewerpartssim.dpatch

 -- Robin Cornelius <robin.cornelius@gmail.com>  Thu, 08 May 2008 13:42:35 +0100

slviewer (1.20.5.0-1) experimental; urgency=low

  * New upstream release

 -- Robin Cornelius <robin.cornelius@gmail.com>  Sat, 03 May 2008 09:07:03 +0100

slviewer (1.20.4.0-1) experimental; urgency=low

  * New upstream release
    + Drop VWR747 as it needs reworking for 1.20.X
  * Build system changes to use debhelper correctly

 -- Secondlife Debian Maintainers <secondlife-debian-maintainers@slupdate.byteme.org.uk>  Wed, 16 Apr 2008 20:46:50 +0100

slviewer (1.19.1.4-4) unstable; urgency=low

  * Bump version and rebuild to wein people of my libcares package
    and get them on to the offical debian libc-ares. I will rebuild
    libc-ares against etch and host on byteme.
  * New bug fixes
    + VWR3857_show_non_latin1_characters_in_lsl_editor.dpatch
    + VWR5636_use_stop_menthod_for_llevent_timer.dpatch
    + VWR5715_text_not_replaced_by_input_with_japanses_im.dpatch
    + VWR-3766_llGetInventoryNumber_tooltip_missing_INVENTORY_ANIMATION
    + VWR1320_buggy_ati_driver_workaround.dpatch 
    + VWR3605_Object_group_display_not_properly_set_for_no-group_objects.dpatch
    + VWR1352_fix_strange_llstyle_copy_operation.dpatch
    + VWR2142_parcel_voice_icon_doesnt_reflect_status_if_voice_is_not_used.dpatch
    + VWR747_texture_resozes_only_proportionately_tp_recieved_dimensions.dpatch 
   

 -- Secondlife Debian Maintainers <secondlife-debian-maintainers@slupdate.byteme.org.uk>  Fri, 25 Apr 2008 09:34:17 +0100

slviewer (1.19.1.4-3) unstable; urgency=low
  * Minor updates and patching
    + Import 4070_openjpeg_check_number_of_components.dpatch
      - Openjpeg fix from Carjay McGinnis
    + Add .desktop file
    + Move icon location to /usr/share/pixmaps/

 -- Secondlife Debian Maintainers <secondlife-debian-maintainers@slupdate.byteme.org.uk>  Wed, 23 Apr 2008 17:47:43 +0100

slviewer (1.19.1.4-2) unstable; urgency=low

  * Minor updates
    + Fix some trademark issues
    + Fix missing message.xml file
    + Fix some dependency problems  

 -- Secondlife Debian Maintainers <secondlife-debian-maintainers@slupdate.byteme.org.uk>  Tue, 22 Apr 2008 21:33:43 +0100

slviewer (1.20.0.0-1) unstable; urgency=low

  * New upstream release

 -- Secondlife Debian Maintainers <secondlife-debian-maintainers@slupdate.byteme.org.uk>  Sat, 05 Apr 2008 09:41:43 +0100

slviewer (1.19.1.3-1) unstable; urgency=low

  * New upstream release

 -- Secondlife Debian Maintainers <secondlife-debian-maintainers@slupdate.byteme.org.uk>  Fri, 28 Mar 2008 07:38:00 +0000

slviewer (1.19.1.2-1) unstable; urgency=low

  * New upstream release
    - Drop patches applied upstream
      + Drop 26_gtk_api_change.dpatch
      + Drop 0000_dont_use_java_prototypes.dpatch

 -- Secondlife Debian Maintainers <secondlife-debian-maintainers@slupdate.byteme.org.uk>  Thu, 20 Mar 2008 19:59:15 +0000

slviewer (1.19.1.1-1) unstable; urgency=low

  * New upstream release

 -- Secondlife Debian Maintainers <secondlife-debian-maintainers@slupdate.byteme.org.uk>  Thu, 13 Mar 2008 14:48:37 +0000

slviewer (1.19.1.0-1) unstable; urgency=low

  * New upstream release
    - Modify 60_use_system_xulrunner
      + We now use mozlib2 and i have moved the component locations to llmozlib2
    - Drop patches applied upstream
      + 0001_no_more_asstachments.patch
      + 3737_fix_gstreamer.dpatch
    - Drop bad patches, TODO Check reason.
      + Drop 0000_minor_leak_slurl_menu_in_llviewertexteditor, VWR3875
      
 -- Secondlife Debian Maintainers <secondlife-debian-maintainers@slupdate.byteme.org.uk>  Sat, 08 Mar 2008 18:55:42 +0000

slviewer (1.19.0.5-1) unstable; urgency=low

  * New upstream release

 -- Secondlife Debian Maintainers <secondlife-debian-maintainers@slupdate.byteme.org.uk>  Sat, 01 Mar 2008 19:28:17 +0000

slviewer (1.19.0.4-1) unstable; urgency=low

  * New upstream release
  * Modify RELEASE_FOR_DOWNLOAD behaviour to ensure all release code paths are used
    - Modify 40_we_sometimes_need_LL_RELEASE_FOR_DOWNLOAD_code_paths.dpatch
    - Modify 21_VWR_2488_Standalone_build_fixes.dpatch 
      + Set gcc flags -DLL_RELEASE_FOR_DOWNLOAD so code paths are correct but scons does
        not know about it and does not try to build tarballs.  
    - Drop 55_use_system_locale.dpatch
      + This patch is still breaking things on locales with , for decimal place. Leave the linden
        code (which works) alone. 
      + Add in 55_fix_the_locales to fix a whole class of bug that only exists here on the Debian 
        builds and not upstream for no aparent reason.

 -- Secondlife Debian Maintainers <secondlife-debian-maintainers@slupdate.byteme.org.uk>  Wed, 27 Feb 2008 07:26:48 +0000

slviewer (1.19.0.3-1) UNRELEASED; urgency=low

  * New upstream release

 -- Secondlife Debian Maintainers <secondlife-debian-maintainers@slupdate.byteme.org.uk>  Tue, 26 Feb 2008 21:42:31 +0000

slviewer (1.19.0.2-2) unstable; urgency=low

  * Maintainence release
    - Fix build for non x86 platforms
      + add 51_fix_vectorize.dpatch
    - Fix locale issue
      + modify 55_use_system_locale.dpatch to use a "C" locale to avoid startup
        crash

 -- Secondlife Debian Maintainers <secondlife-debian-maintainers@slupdate.byteme.org.uk>  Thu, 21 Feb 2008 16:33:07 +0000

slviewer (1.19.0.2-1) unstable; urgency=low

  * New upstream release
    - Make image upload more robust
      + add 4022_jpeg_error_gracefully_2.patch

 -- Secondlife Debian Maintainers <secondlife-debian-maintainers@slupdate.byteme.org.uk>  Fri, 15 Feb 2008 08:55:16 +0000

slviewer (1.19.0.0-1) unstable; urgency=low

  * New upstream release
    - Use our own channel to connect
      + add 79_use_debian_channel.dpatch
    - Drop patches applied upstream
      + drop 2411_possible_crash_in_pipeline.patch.dpatch
      + drop 2412_possible_crash_drawpoolwater_sky.patch.dpatch
      + drop 1769_keyframemotion_llpointer_v3.patch.dpatch
      + drop 1769_undo_linden_llmotions_from_voice.patch.dpatch
      + drop 22_VWR_3458_fix_mouse_cursor_for_bigendian.dpatch
      + drop 23_openjpeg_fixes.dpatch
      + drop 2684_leak_cleanup_layoutstack.patch.dpatch
    - Drop unneeded login patch
      + drop 99_michelle_zenovka_legacy_login_hack.dpatch
    - Drop unneeded paatch due to upstream changes
      + 27_VWR4014_html_files_do_not_have_language_subfolders.dpatch

 -- Secondlife Debian Maintainers <secondlife-debian-maintainers@slupdate.byteme.org.uk>  Thu, 07 Feb 2008 09:19:46 +0000

slviewer (1.18.6.4-2) unstable; urgency=high

  * Security Release
    - Fix possible command injection attack when launching external URL's in a browser

 -- Secondlife Debian Maintainers <secondlife-debian-maintainers@slupdate.byteme.org.uk>  Sat, 19 Jan 2008 19:31:41 +0000

slviewer (1.18.6.4-1) unstable; urgency=low

  * New upstream release
    - Edit 21_VWR2488_standalong_build_fixes
      + Some of this patch is now included upstream
      
 -- Secondlife Debian Maintainers <secondlife-debian-maintainers@slupdate.byteme.org.uk>  Wed, 16 Jan 2008 10:37:18 +0000

slviewer (1.18.6.2-3) UNRELEASED; urgency=low

  * Mozlib is now upto date
    - drop 99_mozlib_is_out_of_date_again
    - tighten dependency on latest llmozlib

 -- Secondlife Debian Maintainers <secondlife-debian-maintainers@slupdate.byteme.org.uk>  Fri, 04 Jan 2008 16:37:14 +0000

slviewer (1.18.6.2-2) UNRELEASED; urgency=high

  * Security Release and cumulitive patches
    - Fix incorrect login URL for german users
    - Use Debian alternatives for www-browser and use system() with & to fork
      + add 71_use_debian_alternatives_for_www_browser.dpatch
    - html files are not in language subfolders for navigateToLocalPage()  
      + add 27_VWR4014_html_files_do_not_have_language_subfolders.dpatch
    - add slviewer.gconf-defaults to add the secondlife:// protocol handler
    - Stop command line arguments being shown on title bar
      + add 40_we_sometimes_need_LL_RELEASE_FOR_DOWNLOAD_code_paths.dpatch
    - Use gstreamer to handle streaming audio
      + Modify 50_add_openal_audio.dpatch to v2 to achieve streaming audio

 --  Secondlife Debian Team <secondlife-debian-maintainers@slupdate.byteme.org.uk>  Mon, 24 Dec 2007 21:59:37 +0000

slviewer (1.18.6.2-1) UNRELEASED; urgency=low

  * New upstream release
    - Mozlib has been changed API breakage but no public release
      + add 99_mozlib_is_out_of_date_again
    - Tess Linden's loginpage patch has entered upstream
      - remove loginpage parts from
        99_tess_linden_and_michelle_zenovka_big_login_hack rename to 
        99_michelle_zenovka_legacy_login_hack 

 -- Secondlife Debian Team <secondlife-debian-maintainers@slupdate.byteme.org.uk>  Sat, 22 Dec 2007 14:38:09 +0000

slviewer (1.18.6.1-1) UNRELEASED; urgency=low

  * New upstream release
    - No more ASStachments!
      + add 0001_no_more_asstachments
    - Minor leak patch
      + add 0001_minor_leak_slulr_menu_in_llviewertexteditor
    - GTK API change
      + add 26_gtk_api_change
    - Import a missing patch pair to 1769
    - add in legacy login system and Tess Lindens web page redirect option
      + add 99_tess_linden_and_michelle_zenovka_big_login_hack

 -- Secondlife Debian Team <secondlife-debian-maintainers@slupdate.byteme.org.uk>  Thu, 13 Dec 2007 08:54:22 +0000

slviewer (1.18.6.0-1) UNRELEASED; urgency=low

  * New upstream release
    - viewer.cpp is now llappviewer.cpp
      + Rebase 50_add_openAL_audio
      + Rebase 24_always_test_vectorize
    - Fix lossless jpeg decoding 
      + update 23_openjpeg_fixes 
    - Fix build dependencies for cares or c-ares so we can still
      compile on etch and remove dependency on gcc 3.4
    - Stop stats bars from overshooting width
      + Add 0000_keep_stats_bars_from_overshooting
    - Crashlogger is still making compile time noise
      + Update 38_build_opensource_without_crashlogger to remove from modules 
    - Fix gstreamer playback
      + Add 3737_fix_gstreamer
    
 -- Secondlife Debian Team <secondlife-debian-maintainers@slupdate.byteme.org.uk>  Fri, 07 Dec 2007 09:01:17 +0000

slviewer (1.18.5.3-3) unstable; urgency=low

  * Add in additional crash and leak patches
    - 1857_apr_thread_mutex_nested
    - 2003_possible_crash_draganddrop
    - 1294_llworkerthread_when_terminating_program
    - 0001_possible_crash_and_leak_llassetstorage
    - 0001_possible_crash_renderglow

 -- Robin Cornelius <robin.cornelius@gmail.com>  Sat, 01 Dec 2007 09:47:08 +0000

slviewer (1.18.5.3-2) unstable; urgency=low

  * Added in various patches for crash and leak prevention
    - 2003_possible_crash_draganddrop
    - 2411_possible_crash_in_pipeline
    - 2412_possible_crash_drawpoolwater_sky
    - 2543_possible_crash_in_group_voting_propsals
    - 2682_possible_crash_dead_cubemap
    - 2683_possible_crash_update_speaker_list
    - 2684_leak_cleanup_layoutstack
    - 2685_possible_crash_hud_bounding_box
    - 1769_keyframemotion_llpointer_v3
    - 0001_texture_cache_hiccups.v2
    - 1857_apr_thread_mutex_nested

 -- Robin Cornelius <robin.cornelius@gmail.com>  Fri, 30 Nov 2007 22:20:56 +0000

slviewer (1.18.5.3-1) unstable; urgency=low

  * New upstream release 1.18.5.3
    - VWR-3458 Fix mouse cursor on bigendian systems
      + Add 22_VWR_3458_fix_mouse_cursor_for_bigendian.dpatch

 -- Robin Cornelius <robin.cornelius@gmail.com>  Fri, 30 Nov 2007 09:38:56 +0000

slviewer (1.18.5.2-1) unstable; urgency=low

  * New upstream release
    Use cares version 1.5.1 - needs patch due to API/ABI change
    Keep using my libcares as i can't do etch builds without it

 -- Robin Cornelius  <robin.cornelius@gmail.com>  Tue, 27 Nov 2007 09:49:58 +0000

slviewer (1.18.5.1-1) unstable; urgency=low

  * New upstream release
    - VWR-2488 Upstream update
      + Update 21_VWR_2488_Standalone_build_fixes based on upstream JIRA bug 
        update.

 -- robin <robin@cornelius.demon.co.uk>  Tue, 20 Nov 2007 09:20:37 +0000

slviewer (1.18.5.0-2) unstable; urgency=low

  * Fix dependency on data and artwork, update to require 1.18.5.0 of both.

 -- Robin Cornelius <robin.cornelius@gmail.com>  Wed, 14 Nov 2007 22:23:22 +0000

slviewer (1.18.5.0-1) unstable; urgency=low

  * New upstream release
    - VWR_2834 fixed upstream
      + Drop 20_VWR_2834_Build_fails_with_no_mozlib.dpatch

 -- Robin Cornelius <robin.cornelius@gmail.com>  Wed, 14 Nov 2007 17:50:58 +0000

slviewer (1.18.4.3-2) unstable; urgency=low

  * Fix dependency issues on package

 -- Robin Cornelius <robin.cornelius@gmail.com>  Tue, 13 Nov 2007 18:56:25 +0000

slviewer (1.18.4.3-1) unstable; urgency=low

  * New upstream release
    - VWR-1987 fixed upstream
      + Drop 20_VWR-1987_dont_crash_twiddling_nonexistent_volume_settings
    - VWR-1598 fixed upstream
      + Drop VWR-1598_fallback_to_homegrown_ns_parser
    - Standalone build fails with mozlib=no
      + Add 20_VWR_2834_Build_fails_with_no_mozlib
    - Packaging fails for AMD64 systems
      + Add 21_VWR_2488_Standalone_build_fixes
    - Openjpeg lossess encoding bug and permit use of openjpeg >1.2
      + Add 23_openjpeg_fixes
    - Dates and times are corrupt on AMD64 systems
      + Add 24_fix_64bit_times
    - Rebased existing patches for current release
      + 34_dont_hide_symbols
      + 38_build_opensource_without_crashlogger
      + 60_use_system_headers_for_apr-1  
      + 68_use_system_headers_for_openjpeg
      + 75_use_debian-included_fonts
    - Improved upstream build process
      + Drop 44_pkgconfig_me_harder
      + Drop 30_vectorisation_for_all 
      + Drop 40_know_your_distribution
  
    - Debug packet log code included upstream
      + Drop 53_more_detail_in_logRanOffEndOfPacket
    
  * cares is being packaged for debian and there are API differences that
    areas cannot provide
      + Drop 45_libcares_not_available_use_libares_instead

  * LLMozLib is being packaged for debian using prebuilt xulrunner packages 
      + Add 60_use_system_xulrunner

  * Sound is now provided via OpenAL
      + Add 50_add_openAL_audio

 -- Robin Cornelius <robin.cornelius@gmail.com>  Fri, 09 Nov 2007 15:24:09 +0000

slviewer (1.18.1.2-1) unstable; urgency=low

  * New upstream release
    - VWR-1585 fixed upstream
      + Drop 20_VWR-1585_remove_extra_qualification
    - VWR-937 fixed upstream
      + Drop 32_remove_unused_db-4.2_link
    - Client doesn't build code to call crashlogger by default
      + Removed similar change from 38_build_opensource_without_crashlogger
    - More pkgconfig usage upstream
      + Updated 44_pkgconfig_me_harder
    - Back to building all libraries static in opensource build
      + Drop 49_shared_libraries_are_good
    - Use system expat headers in standalone build
      + Drop 62_use_system_headers_for_expat
    - Use system zlib headers in standalone build
      + Drop 64_use_system_headers_for_zlib
    - Use system libjpeg headers in standalone build
      + Drop 66_use_system_headers_for_libjpeg
    - Use system freetype headers in standalone build
      + Drop 69_use_system_headers_for_freetype
    - Media streaming support using gstreamer
      + Updated 44_pkgconfig_me_harder
    - SSE/SSE2-optimised compilation of certain speed-critical routines
      + Add 30_vectorisation_for_all to fix build on PowerPC, requires
        Altivec or package rebuild on PowerPC.
    - Voice support
      + Add 52_standalone_expat_headers to fix path to expat headers

  * Refreshed dpatches
    - Trimmed 50_get_cpu_clock_count_for_more_than_just_i386 to just make
      everything use gettimeofday, so at least this stuff's correct, even
      if it's not directly using CPU performance counters to squeeze every
      last ounce of speed out.

   * Disabled symbol hiding for the slviewer binary when we don't staticly
     link as much as the upstream build does
     - Add 34_dont_hide_symbols

   * Fix crash on connecting to Second Life grid, VWR-1987 upstream
     - Add 20_VWR-1987_dont_crash_twiddling_nonexistent_volume_settings

 -- Paul "TBBle" Hampson <Paul.Hampson@Pobox.com>  Sat, 11 Aug 2007 11:12:27 +1000

slviewer (1.18.0.6-1) unstable; urgency=low

  * New upstream release
    - libgoogle-perftools-dev are only used in debug builds, which we don't
      do in the debian build script
    - pkgconfig support added
      + Drop 30_use_default_gcc
      + Drop 34_pkgconfigise_SConstruct
      + Add 44_pkgconfig_me_harder, pkgconfig more libraries
    - ELFIO made optional
      + Drop 36_make_ELFIO_optional
    - xmlrpc-epi recognised as library name on Fedora
      + Drop 40_libxmlrpc-epi_is_libxmlrpc-epi
      + Add 40_know_your_distribution, detect Debian
      + Add 42_debian_also_uses_-epi, link xmlrpc-epi
    - fix for unexercised codepath in template message processor
      + Drop 52_four_bytes_does_not_go_into_one_byte
    - correct occasional "" inclusions of boost to <>
      + Drop 57_use_system_boost_everywhere
    - use default g++ when building with system libraries
      + Add 20_VWR-1585_remove_extra_qualification, gcc4-misliked syntax fix
    - support SRV DNS record lookups
      + Add 22_VWR-1598_fallback_to_homegrown_ns_parser, libresolv-based
        parser expects bind8 libresolv
    - build some libraries shared
      + Add 49_shared_libraries_are_good, to build the rest shared, and add
        the neccessary rpath to the executable to find them on installation

  * Refreshed dpatches
    - Disbled 53_more_detail_in_logRanOffEndOfPacket, only used for debugging
      and not generally useful as message data order does not match template

  * Clean up some stray pyc files built during template testing

 -- Paul "TBBle" Hampson <Paul.Hampson@Pobox.com>  Sun, 22 Jul 2007 15:01:24 +1000

slviewer (1.17.0.12-2) unstable; urgency=low

  * Build using default g++ (currently 4.1)
  * Correct 'lubcurl3-openssl-dev' => 'libcurl3-openssl-dev' in Build-Depends
  * Refreshed dpatches

 -- Paul "TBBle" Hampson <Paul.Hampson@Pobox.com>  Sat, 16 Jun 2007 12:07:09 +1000

slviewer (1.17.0.12-1) unstable; urgency=low

  * New upstream release
    - Now requires libgoogle-perftools-dev to build, except on AMD64
  * Add lubcurl3-openssl-dev as an alternative build-depends to
    libcurl4-openssl-dev, to ease Etch backporting.
    - Thanks to Rainer Dorsch for the suggestion

 -- Paul "TBBle" Hampson <Paul.Hampson@Pobox.com>  Fri, 15 Jun 2007 10:46:02 +1000

slviewer (1.16.0.5-1) unstable; urgency=low

  * Initial release (Closes: #406335, #406185)

 -- Paul "TBBle" Hampson <Paul.Hampson@Pobox.com>  Fri, 25 May 2007 00:02:48 +1000


